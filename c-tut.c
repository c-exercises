/* Die Wiedergabe des folgenden sowie weiterer Beispiele erfolgt mit freundlicher Genehmigung von Galileo Press GmbH.
http://www.galileo-press.de
http://www.galileocomputing.de/katalog/openbook/?GPP=opc3 */

/* steuerzeichen.c */ 
#include <stdio.h>

int main(void) {
   printf("Ein akustisches Signal mit : (\\a)\a");
   printf("\nEin Backspace mit : (\\b) | \bx\n");
   printf("Ein Zeilenvorschub mit : (\\t) |\tx");
   printf("\n\tC\n\ti\n\ts\n\tt\n\ttoll\n");
   printf("\t   u\n\t   n\n\t   d\n");
   printf("\t   macht Spaß\n");
   return0;
}

/* Übungsvorschlag: die wiederholten Zeilenvorschübe durch Schleifen ersetzen. */
